package wantsome.project.ui.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.FullReservationDto;
import wantsome.project.db.service.ReservationDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.ui.web.SparkUtil.render;

public class ReservationsPageController {

    private static final ReservationDao reservationDao = new ReservationDao();
    public static String showReservationsPage(Request req, Response res) {

        // hide past reservations
        boolean hidePastReservations = getHideOldReservationsFromParamOrSes(req);
        List<FullReservationDto> allReservations = reservationDao.getAllOrderedByStartDate();
        List<FullReservationDto> activeReservations = reservationDao.getActiveReservationsOrderedByDate();
        Map<String, Object> model = new HashMap<>();

        long activeCount = activeReservations.stream().count();
        long oldReservationsCount = allReservations.size() - activeCount;

        if (hidePastReservations) {
            model.put("reservations", activeReservations);
        } else {
            model.put("reservations", allReservations);
        }

        model.put("activeCount", activeCount);
        model.put("oldReservationsCount", oldReservationsCount);
        model.put("hidePastReservations", hidePastReservations);

        return render(model, "reservations.vm");
    }

    public static String showReservationsPageAfterSearch(Request req, List<FullReservationDto> reservations) {

        boolean hidePastReservations = getHideOldReservationsFromParamOrSes(req);
        List<FullReservationDto> allReservations = reservationDao.getAllOrderedByStartDate();
        List<FullReservationDto> activeReservations = reservationDao.getActiveReservationsOrderedByDate();
        Map<String, Object> model = new HashMap<>();

        long activeCount = activeReservations.stream().count();
        long oldReservationsCount = allReservations.size() - activeCount;

        String searchParam = req.queryParams("search");
        boolean searchPage = searchParam != null;

        model.put("activeCount", activeCount);
        model.put("oldReservationsCount", oldReservationsCount);
        model.put("hidePastReservations", hidePastReservations);
        model.put("search", searchParam);
        model.put("reservations", reservations);
        if (reservations.isEmpty()) {
            model.put("errorMsg", "No reservation found under the name of " + searchParam);
        }
        model.put("searchPage", searchPage);
        return render(model, "reservations.vm");
    }

    public static String handleSearchRequest(Request req, Response res) {
        Map<String, Object> model = new HashMap<>();
        String searchParam = req.queryParams("search");
        model.put("search", searchParam);
        try {
            List<FullReservationDto> reservations = reservationDao.getReservationByClientsName(searchParam);
            return showReservationsPageAfterSearch(req, reservations);
        } catch (Exception e) {
            return showReservationsPage(req, res);
        }
    }

    private static boolean getHideOldReservationsFromParamOrSes(Request req) {
        String param = req.queryParams("hidePastReservations"); //read it from current request (if sent)
        if (param != null) {
            req.session().attribute("hidePastReservations", param);//save it to session for later
        } else {
            param = req.session().attribute("hidePastReservations"); //try to read it from session
        }
        //default value (first time)
        return param != null && (param.equals("true"));
    }

    public static Object handleDeleteRequest(Request req, Response res) {
        String id = req.params("id");
        try {
            reservationDao.delete(Long.parseLong(id));
        } catch (Exception e) {
            throw new RuntimeException("Error deleting reservation with id '" + id + "': " + e.getMessage());
        }
        res.redirect("/main");
        return res;
    }
}