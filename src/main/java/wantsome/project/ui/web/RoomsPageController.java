package wantsome.project.ui.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.RoomDto;
import wantsome.project.db.service.RoomDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.ui.web.SparkUtil.render;

public class RoomsPageController {

    private static final RoomDao roomsDao = new RoomDao();

    public static String showRoomsPage(Request req, Response res) {
        Map<String, Object> model = new HashMap<>();
        List<RoomDto> allRooms = roomsDao.getAll();
        model.put("rooms", allRooms);
        return render(model, "rooms.vm");
    }

    public static Object handleDeleteRequest(Request req, Response res) {
        String number = req.params("number");
        Map<String, Object> model = new HashMap<>();
        List<RoomDto> allRooms = roomsDao.getAll();
        try {
            roomsDao.delete(Long.parseLong(number));
        } catch (Exception e) {
            model.put("errorMsg", "Error deleting room nr " + number + ". This room might already be reserved. ");
            model.put("rooms", allRooms);
            return render(model, "rooms.vm");
        }
        res.redirect("/rooms");
        return res;
    }
}